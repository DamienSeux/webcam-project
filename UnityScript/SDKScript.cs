﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SDKScript : MonoBehaviour {

	public AndroidJavaObject sdkInterface;
	public GameObject plane;

	private IntPtr jAryPtr;

	// Use this for initialization
	void Start () {
		Debug.Log ("Hello", gameObject);

		Debug.Log("Running from android");
		AndroidJavaObject activityContext = null;

		AndroidJNIHelper.debug = true;
		if(activityContext == null) {
			using(AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
			activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			}
			using(AndroidJavaClass sdkInterfaceClass = new AndroidJavaClass("com.kolibree.webcamprojet.seeklop_sdk.SDKInterface")){
			sdkInterface = sdkInterfaceClass.CallStatic<AndroidJavaObject>("instance");
			}
			if (sdkInterface != null) {
				sdkInterface.Call("setActivity",activityContext);
				sdkInterface.Call("init");

				IntPtr localRef = AndroidJNI.NewByteArray (960 * 540 * 4);
				jAryPtr = AndroidJNI.NewGlobalRef(localRef);
			} else {
					Debug.Log("SDKInterface object was null");
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("Start update");
		if (sdkInterface == null) {
			Debug.Log("SdkInterface is null");
		}

		IntPtr methodId = AndroidJNIHelper.GetMethodID(sdkInterface.GetRawClass(), "getPicture");
		Debug.Log ("Before calling getPicture");

		jvalue[] blah = new jvalue[1];
		blah [0].l = jAryPtr;

		AndroidJNI.CallVoidMethod(sdkInterface.GetRawObject(), methodId, blah);
		Debug.Log ("After calling getPicture");
		byte[] result = AndroidJNI.FromByteArray (jAryPtr);
		Debug.Log ("After converting to float array");


		if (result.Length > 0 || result[3] != 0 ) {									// alpha pixels are set to 255 in valid arrays
			Debug.Log ("Creating texture");
			Texture2D tex = new Texture2D (960, 540, TextureFormat.RGBA32, false);
			Debug.Log ("Loading texture data");
			tex.LoadRawTextureData (result);
			tex.filterMode = FilterMode.Bilinear;
			Debug.Log ("Applying texture");
			tex.Apply ();
			Debug.Log ("Texture applied");
			plane.GetComponent<Renderer> ().material.mainTexture = tex;
		} else {
			Debug.Log ("Received null array");
		}
	
	}
}
