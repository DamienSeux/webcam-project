package com.kolibree.webcamprojet.seeklop_sdk;

import android.graphics.SurfaceTexture;
import android.view.SurfaceHolder;

/**
 * Created by Dawid on 2016-08-27.
 */
public interface CameraPlayer {
    void stopPreview();

    void initCamera(SurfaceHolder holder);

    void initCamera(SurfaceTexture sTexture);

    void startPreview();


}
