package com.kolibree.webcamprojet.seeklop_sdk;

/**
 * Created by Dawid on 2016-08-27.
 */
public interface CameraScreen {

    void setCameraPlayer(CameraPlayer cameraPlayer);

    int getWidth();

    int getHeight();

    void removePlayer();
}
