package com.kolibree.webcamprojet.seeklop_sdk;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.util.Log;


import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;


public class SDKInterface implements CameraControl.OnPreviewFrameListener {
    private final static String LOG_TAG = "SDKInterface";
    private static final int CAMERA_REQUEST_CODE = 123;
    public static SDKInterface instance;
    private CameraControl mCameraControl;
    private Activity activity;
    private CameraSurfaceView cameraSurfaceView;
    int height;
    int width;
    byte[] lastPictureData;
    boolean receivedFirst;
    final Lock lock;

    public SDKInterface(){
        this.lock = new ReentrantLock();
    }

    public static SDKInterface instance(){
        if(instance == null) {
            instance =  new SDKInterface();
        }
        return instance;
    }


    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void init(){
        if(activity == null){
            Log.e(LOG_TAG,"Called init without setting non-null activity");
        }
        Log.i(LOG_TAG,"Started init");
        receivedFirst = false;
        checkCameraPermission();
        cameraSurfaceView = new CameraSurfaceView(activity);
        initCameraDetection(Camera.CameraInfo.CAMERA_FACING_FRONT);
    }

    public void stop(){
        mCameraControl.stopCamera();
    }

    public void getPicture(byte[] data) {
        if(!receivedFirst){
            Arrays.fill(data,(byte)255);
            return;
        }
        Log.i("Unity", "Byte[]: " + data + " and length: " + data.length);
        if(data.length<4 * width * height)
            throw new RuntimeException("Array was too short");

        Log.i(LOG_TAG, "Start rendering script");
        renderScriptNV21ToRGBA888(activity, width, height, lastPictureData, data);
        Log.i(LOG_TAG, "Rendering script complete");
        return ;
    }

    private void checkCameraPermission() {
        if(Build.VERSION.SDK_INT>=23) {
            if (activity.checkSelfPermission(Manifest.permission.CAMERA) != PERMISSION_GRANTED)
                activity.requestPermissions(
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
        }
    }

    protected void initCameraDetection(int cameraOrientation) {
        mCameraControl = new StandardApiCameraControl(this.activity, cameraOrientation);
        mCameraControl.setOnPreviewFrameListener(this);
        startCamera();
    }

    private void startCamera() {
        Log.d(LOG_TAG, "startCamera");
        mCameraControl.setCameraScreen(cameraSurfaceView);
        mCameraControl.startCamera();
    }

    public void renderScriptNV21ToRGBA888(Context context, int width, int height, byte[] nv21,byte[] outByte) {
        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicYuvToRGB yuvToRgbIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));

        Type.Builder yuvType = new Type.Builder(rs, Element.U8(rs)).setX(nv21.length);
        Allocation in = Allocation.createTyped(rs, yuvType.create(), Allocation.USAGE_SCRIPT);
        Type.Builder rgbaType = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(width).setY(height);
        Allocation out = Allocation.createTyped(rs, rgbaType.create(), Allocation.USAGE_SCRIPT);

        in.copyFrom(nv21);

        yuvToRgbIntrinsic.setInput(in);
        yuvToRgbIntrinsic.forEach(out);
        out.copy1DRangeTo(0,width*height,outByte);
    }

    @Override
    public void onPreviewFrame(byte[] data, int width, int height) {
        synchronized (lock) {
            receivedFirst = true;
            this.width = width;
            this.height = height;
            this.lastPictureData = data;
        }
    }
}
