package com.kolibree.webcamprojet.seeklop_sdk;

import android.content.Context;

/**
 * Created by Dawid on 2016-08-27.
 */
public abstract class CameraControl {

    private int mCameraOrientation;
    protected OnPreviewFrameListener mOnPreviewFrameListener = new NullOnPreviewFrameListener();
    public Context mContext;

    public CameraControl(Context context) {
        mContext = context;
    }

    public void setOnPreviewFrameListener(OnPreviewFrameListener onPreviewFrameListener) {
        if (onPreviewFrameListener != null)
            mOnPreviewFrameListener = onPreviewFrameListener;
        else
            mOnPreviewFrameListener = new NullOnPreviewFrameListener();
    }

    public int getCameraOrientation() {
        return mCameraOrientation;
    }

    public void setCameraOrientation(int cameraOrientation) {
        mCameraOrientation = cameraOrientation;
    }

    public abstract void setCameraScreen(CameraScreen cameraScreen);

    public abstract void startCamera();

    public abstract void stopCamera();

    abstract int getImageWidth();

    abstract int getImageHeight();

    public abstract boolean isBackCamera();

    class NullOnPreviewFrameListener implements OnPreviewFrameListener {

        @Override
        public void onPreviewFrame(byte[] data, int width, int height) {

        }
    }

    interface OnPreviewFrameListener {
        void onPreviewFrame(byte[] data, int width, int height);
    }
}
