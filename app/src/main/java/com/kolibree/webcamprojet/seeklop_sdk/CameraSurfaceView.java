package com.kolibree.webcamprojet.seeklop_sdk;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by Dawid on 2016-08-27.
 */
public class CameraSurfaceView extends CustomSurfaceView implements CameraScreen, ImageDataAdapter {

    private static final String LOG_TAG = "CameraView";
    private CameraPlayer mCameraPlayer;

    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraSurfaceView(Context context) {
        super(context);
    }


    public void setCameraPlayer(CameraPlayer cameraPlayer) {
        mCameraPlayer = cameraPlayer;
        mHolder.addCallback(CameraSurfaceView.this);
        Log.i(LOG_TAG, "setCameraPlayer");
        if (mCameraPlayer != null)
            start(mHolder);
    }

    @Override
    public void removePlayer() {
        mHolder.removeCallback(CameraSurfaceView.this);
        mCameraPlayer.stopPreview();

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        Log.i(LOG_TAG, "surfaceCreated");
        if (mCameraPlayer == null)
            return;
        start(holder);
    }

    private void start(SurfaceHolder holder) {
        Log.i(LOG_TAG, "start");
        mCameraPlayer.initCamera(holder);
        mCameraPlayer.startPreview();
    }

    @Override
    public void drawData(byte[] data, int width, int height) {

    }
}