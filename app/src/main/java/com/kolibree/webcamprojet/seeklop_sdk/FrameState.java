package com.kolibree.webcamprojet.seeklop_sdk;


import android.graphics.Bitmap;

import java.nio.ByteBuffer;


public class FrameState {

    private byte[] mData;
    private int mWidth;
    private int mHeight;
    private int mCameraOrientation;
    private ByteBuffer mDataBuffer;
    private Bitmap mBeginBitamp;
    private boolean mIsReversed;

    public boolean[] getPosition() {
        return mPosition;
    }

    public void setPosition(boolean[] mPosition) {
        this.mPosition = mPosition;
    }

    private boolean[] mPosition;

    public void setData(byte[] data) {
        mData = data;
    }

    public byte[] getData() {
        return mData;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getCameraOrientation() {
        return mCameraOrientation;
    }

    public void setCameraOrientation(int cameraOrientation) {
        mCameraOrientation = cameraOrientation;
    }

    public void setDataBuffer(ByteBuffer dataBuffer) {
        mDataBuffer = dataBuffer;

    }

    public ByteBuffer getDataByteBuffer() {
        if (mDataBuffer == null)
            mDataBuffer = ByteBuffer.wrap(mData);
        return mDataBuffer;
    }

    public Bitmap getBeginBitmap() {
        return mBeginBitamp;
    }

    public void setBeginBitmap(Bitmap bitmap) {
        mBeginBitamp = bitmap;

    }

    public void setIsReversed(boolean isReversed) {
        mIsReversed = isReversed;
    }

    public boolean isReversed() {
        return mIsReversed;
    }
}
