package com.kolibree.webcamprojet.seeklop_sdk;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by Dawid on 2016-08-27.
 */
public class StandardApiCameraControl extends CameraControl implements Camera.PreviewCallback {
    private final static String LOG_TAG = "StandardApiCameraContr";
    private final static int FRAME_RATE = 30;
    private final static int DEFAULT_IMAGE_WIDTH = 960;
    private final static int DEFAULT_IMAGE_HEIGHT = 540;
    private static int mCamerOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
    /* video lastPictureData getting thread */
    private Camera mCamera;
    private boolean isPreviewOn;
    private CameraScreen mCameraScreen;
    private int imageWidth;
    private int imageHeight;

    public StandardApiCameraControl(Context context) {
        super(context);
    }

    public StandardApiCameraControl(Context context, int cameraOrientation) {
        super(context);
        mCamerOrientation = cameraOrientation;
    }

    @Override
    public void setCameraScreen(CameraScreen cameraScreen) {
        mCameraScreen = cameraScreen;
    }


    @Override
    public int getImageWidth() {
        return imageWidth;
    }

    @Override
    public int getImageHeight() {
        return imageHeight;
    }

    @Override
    public boolean isBackCamera() {
        return mCamerOrientation == Camera.CameraInfo.CAMERA_FACING_BACK;
    }

    @Override
    public void startCamera() {
        Log.d(LOG_TAG, "startCamera");
        mCamera = Camera.open(getCameraInfoIndex());
        mCameraScreen.setCameraPlayer(new CameraPlayer() {
            @Override
            public void stopPreview() {
                Log.d(LOG_TAG, "stopPreview");
                StandardApiCameraControl.this.stopPreview();

            }

            @Override
            public void initCamera(SurfaceHolder holder) {
                Log.d(LOG_TAG, "initCamera-Holder " + holder);
                StandardApiCameraControl.this.initCamera();
                try {
                    mCamera.setPreviewDisplay(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void initCamera(SurfaceTexture sTexture) {
                Log.d(LOG_TAG, "initCamera-Surface");
                StandardApiCameraControl.this.initCamera();
                try {
                    mCamera.setPreviewTexture(sTexture);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void startPreview() {
                Log.d(LOG_TAG, "startPreview");
                StandardApiCameraControl.this.startPreview();
            }

        });
    }

    private void initCamera() {
        Log.d(LOG_TAG, "initCamera");
        Camera.Parameters camParams = mCamera.getParameters();

        Camera.CameraInfo camInfo = getCameraInfo();

        List<Camera.Size> sizes = camParams.getSupportedPreviewSizes();
        for(Camera.Size size : sizes)
            Log.d(LOG_TAG," "+size.height+","+size.width);
        // Sort the list in ascending order
        Collections.sort(sizes, new Comparator<Camera.Size>() {

            public int compare(final Camera.Size a, final Camera.Size b) {
                return a.width * a.height - b.width * b.height;
            }
        });
  /*      imageWidth = mCameraScreen.getWidth();
        imageHeight = mCameraScreen.getHeight();*/

        imageWidth = DEFAULT_IMAGE_WIDTH;
        imageHeight = DEFAULT_IMAGE_HEIGHT;


        // Pick the first preview size that is equal or bigger, or pick the last (biggest) option if we cannot
        // reach the initial settings of imageWidth/imageHeight.
        for (int i = 0; i < sizes.size(); i++) {
            if ((sizes.get(i).width >= imageWidth && sizes.get(i).height >= imageHeight) || i == sizes.size() - 1) {
                imageWidth = sizes.get(i).width;
                imageHeight = sizes.get(i).height;
                Log.i(LOG_TAG, "Changed to supported resolution: " + imageWidth + "x" + imageHeight);
                break;
            }
        }
        camParams.setPreviewSize(imageWidth, imageHeight);

        Log.v(LOG_TAG, "Setting imageWidth: " + imageWidth + " imageHeight: " + imageHeight + " frameRate: " + FRAME_RATE);

        List<Integer> supportedFormats = camParams.getSupportedPreviewFormats();
        for(int i : supportedFormats){
            Log.e(LOG_TAG,"Supported Format : "+i);
        }
        camParams.setPreviewFrameRate(FRAME_RATE);
        Log.e(LOG_TAG,"Base format "+ImageFormat.NV21);
        camParams.setPreviewFormat(ImageFormat.NV21);
        countOrientation(mCamera, camParams, camInfo);
        mCamera.setParameters(camParams);
        mCamera.setPreviewCallback(this);
    }

    private void countOrientation(Camera camera, Camera.Parameters cameraPaR, Camera.CameraInfo cameraInfo) {
        WindowManager windowManager =
                (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        short degrees = 0;
        int rootation = windowManager.getDefaultDisplay().getRotation();
        switch (rootation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int angle;
        int displayAngle;
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            angle = (cameraInfo.orientation + degrees) % 360;
            displayAngle = (360 - angle) % 360; // compensate for it being mirrored
        } else {
            angle = (cameraInfo.orientation - degrees + 360) % 360;
            displayAngle = angle;

        }

        this.setCameraOrientation(angle / 90);

        camera.setDisplayOrientation(displayAngle);
        cameraPaR.setRotation(angle);
    }


    private int getCameraInfoIndex() {
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == mCamerOrientation) {
                return i;
            }
        }
        return -1;
    }

    private Camera.CameraInfo getCameraInfo() {
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == mCamerOrientation) {
                return info;
            }
        }
        return null;
    }

    @Override
    public void stopCamera() {
        Log.d(LOG_TAG, "stopCamera");
        if (mCameraScreen != null) {
            mCameraScreen.removePlayer();
            mCameraScreen = null;
        }
        if (mCamera != null) {
            stopPreview();
            mCamera = null;
        }
    }

    public void startPreview() {
        Log.d(LOG_TAG, "startPreview " + isPreviewOn + " " + (mCamera != null));
        if (!isPreviewOn && mCamera != null) {
            Log.d(LOG_TAG, "startPreview-release");
            isPreviewOn = true;
            mCamera.startPreview();
        }
    }

    public void stopPreview() {
        if (isPreviewOn && mCamera != null) {
            Log.d(LOG_TAG, "stopPreview-release");
            isPreviewOn = false;

            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
        }
    }


    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        int width = parameters.getPreviewSize().width;
        int height = parameters.getPreviewSize().height;
        mOnPreviewFrameListener.onPreviewFrame(data, width, height);
    }


}
