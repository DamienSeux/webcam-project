package com.kolibree.webcamprojet.seeklop_sdk;

/**
 * Created by Dawid on 2016-08-28.
 */
public interface ImageDataAdapter {
     void drawData(byte[] data, int width, int height);
}
